const express = require("express");

//"mongoose" is a package that allows creation of schemas to model our data structures
// Also has access to a number of methods for manipulating our database
const mongoose = require("mongoose");


// Create an application using express
// "app" will serve as our server
const app = express();

const port = 3001;

// Connecting to MongoDB Atlas

mongoose.connect("mongodb+srv://kevinpatrickmercadal:1QAFe83REUGaONKN@wdc028-course-booking.qfbph5p.mongodb.net/", 
	{
		// Everytime we add MongoDB connection string, we should always add the code below in order to avoid future errors
		useNewUrlParser: true,
		useUnifiedTopology: true
	}

);

// Connecting to MongoDB locally

let db = mongoose.connection;

// "console.error.bind" allows us to print errors in the browser console and in the terminal
// "connection error" is the message that will display if an error is encountered
db.on("error", console.error.bind(console, "connection error"));

// If the connection is successful, "We're connected to the cloud database!" output in the console
db.once("open", ()=> console.log("We're connected to the cloud database!"));



// Middleware
app.use(express.json());
app.use(express.urlencoded({extended: true}));


// [SECTION] Mongoose Schemas
// Schemas determine the structure of the documents to be written in the database
// Schemas act as blueprints to our data

// The "new" keyword creates a new schema
// Use the Schema() constructor of the mongoose module to create a new schema object
/*
Syntax: 
	const schemaName = new mongoose.Schema({})
// */
// const taskSchema = new mongoose.Schema({
// 	name: String,
// 	status: {
// 		type: String,
// 		default: "pending"
// 	}
// })

// [SECTION] Models
// Models must be in singular form and first letter is capitalized
// First paramter of mongoose model method indicates the collection whrein we store the data
// Second parameter is used to specify the scheme or blueprint of the documents that will be stored in the MongoDB collection'
/*
Syntax: 
	const Model = mongoose.model("collectionName", schemaName);
*/
// const Task = mongoose.model("Task", taskSchema);

/*
Creating a new task
1. Add a functionality to check if there are duplicate tasks
	- If the task already exists in the database, we return a message
	- If the task doesn't exist in the database, we add it in the database
2. The task data will be coming from the request's body
3. Create a new Task object with a "name" field/property
4. The "status" property does not need to be provided because our schema defaults it to "pending" upon creation of an object
*/

// app.post("/tasks", (req,res) =>{
// 	Task.findOne({name: req.body.name}).then((result, err)=>
// 		{
// 		if (result !== null && result.name == req.body.name) {
// 			return res.send("Duplicate task found!");
// 		}else {
// 			let newTask = new Task ({
// 				name: req.body.name
// 			});
// 			// ".save()" method will store the information to the database
// 			newTask.save(). then((savedTask, saveErr) => {
// 				if(saveErr) {
// 					return console.error(saveErr);
// 				} else {
// 					return res.status(201).send("New Task created");
// 				}
// 			})
// 		}
// 		})
// })


/*
Getting all the tasks
1. Retrieve all the documents
2. If an error is encountered, print the error
3. If no errors are found, send a success status back to the client/Postman and return an array of documents
*/

// app.get("/tasks", (req,res) => {
// 	Task.find({}).then((result, err) => {
// 		if(err) {
// 			return console.log(err);
// 		} else {
// 			return res.status(200).json({
// 				data: result
// 			})
// 		}
// 	})

// });



/*
[SECTION] Activity s35
*/

// User Schema
const userSchema = new mongoose.Schema({
	username: String,
	password: String
});

//Model

const User = mongoose.model("User", userSchema);

// Create a User

app.post("/signup", (req,res) =>{
	User.findOne({username: req.body.username}).then((result, err)=>
		{
		if (result !== null && result.username == req.body.username) {
			return res.send("Duplicate username found!");
		}
		else if(req.body.username !== null && req.body.password!==null){
			let newUser = new User ({
				username: req.body.username,
				password: req.body.password
			});
			// ".save()" method will store the information to the database
			newUser.save(). then((savedUser, saveErr) => {
				if(saveErr) {
					return console.error(saveErr);
				} else {
					return res.status(201).send("New User Registered");
				}
			})
		}
		else {
			return res.send("Both username and password must be provided.")
		}
		})
})




app.listen(port, () => console.log(`Server running at port ${port}`));