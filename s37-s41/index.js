const express = require("express");
const mongoose = require("mongoose");

const app = express();

// Allows our backend application to be available  to our frontend application
const cors = require("cors");

const userRoute = require("./routes/userRoute");

const courseRoute = require("./routes/courseRoute");

// Connecting to MongoDB Atlas
mongoose.connect("mongodb+srv://kevinpatrickmercadal:1QAFe83REUGaONKN@wdc028-course-booking.qfbph5p.mongodb.net/courseBookingAPI",
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);

// Connecting to MongoDB locally
mongoose.connection.once("open", () => console.log("We're connected to the cloud database!"));

// Middleware
app.use(express.json());
app.use(express.urlencoded({extended: true}));
// Allows all resources to access our backend application
app.use(cors());

// Defines the "/users" srtring to be included for all user routes defined in the "userRoute" file
app.use("/users", userRoute);

// Defines the "/courses" string to be included for all course routes defined in the "course" route file
// http://localhost:4000/courses
app.use("/courses", courseRoute);

// "process.env.PORT" is an environment variable that typically holds the port number on which the server should listen.
app.listen(process.env.PORT || 4000, () => console.log(`Now listening to port ${process.env.PORT || 4000}!`));