const jwt = require("jsonwebtoken");


//User defined string data that will be used to create our JSON webtokens
const secret = "CourseBookingAPI";


//[SECTION] JSON Web Token
// jsonwebtoken - is a way for us to securely pass information from backend to frontend

// Token Creation
module.exports.createAccessToken = (user) => {
	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	};

	//GEnerate a JSON web token using the jwt's sign method
	// Generates the token using the form data and the secret code wth no additional options provided
	return jwt.sign(data, secret, {});
};


// TOKEN VERIFICATION

module.exports.verify =(req, res, next) => {
	let token = req.headers.authorization;

	if(typeof token !== "undefined") {
		console.log(token);
		token = token.slice(7, token.length);
		return jwt.verify(token, secret, (err, data) => {
			if(err){
				return res.send({auth: failed})

			} else {
				next();
			}

		})
	} else {
		return res.send({auth:"failed"})
	};
};

// Token decryuption

module.exports.decode = (token) => {
	if(typeof token !== "undefined") {
		token = token.slice(7, token.length);
		return jwt.verify(token, secret, (err, data) => {
				if(err) {
					return null
				} else {
					return jwt.decode(token, {complete:true}).payload;
				}
		})
	} else {
		return null;
	};
};