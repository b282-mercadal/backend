// Defines WHEN particulat controllers will be usedd
// Contains all the endpoints for our application

const express = require("express");

// Creates a router instance that functions as a middleware and routing system
const router = express.Router();

// The "taskController" allows us to use the functions defined in the "taskController.js" file
const taskController = require("../controllers/taskController")

// [SECTION] Routes
// http://localhost:3001/tasks/
// Thjis will no longer be "3001/" since we already declared in the server that all URI in taskRoute will be contained in "/tasks"
router.get("/", (req,res)=>{
	// Invokes the "getAllTasks" function from the "controller.js" file and sends the result back to the clien/Postman
	taskController.getAllTasks().then(resultFromController => res.send(resultFromController));
});

// Post 
router.post("/", (req,res)=>{
	taskController.createTask(req.body).then(resultFromController => res.send(resultFromController));
});

// Delete Task

/*
Business Logic
1. Look for the task with the corresponding id provided in the URL/route
2. Delete the task using the Mongoose method "findByIdAndRemove" with the same id provided in the route
*/


// "/:" creates a dynamic route - it points out that the object next to it can have different valuees, most of the times ID is being used
router.delete("/:id", (req,res)=>{
	taskController.deleteTask(req.params.id).then(resultFromController => res.send(resultFromController));
});


//Updating a Task
router.put("/:id",(req,res) =>{
	taskController.updateTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController));
});

//[SECTION] Activity s36

// Get specific task

router.get("/:id", (req,res)=>{
	taskController.getSpecificTask(req.params.id).then(resultFromController => res.send(resultFromController));
});

// Update specific task

router.put("/:id/complete",(req,res) =>{
	taskController.updateSpecificTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController));
});




module.exports = router;