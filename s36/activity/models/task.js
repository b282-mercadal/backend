// Contains WHAT objects are needed in our API
// We still need to require mongoose since we already have separated it from the server
// If we dont require it here, it will become undefined

const mongoose = require("mongoose");

const taskSchema = new mongoose.Schema({
	name: String,
	status: {
		type: String,
		default: "pending"
	}
});


// "module.exports" is a way for Node JS to treat this value as a "package" that can be used by other files
module.exports = mongoose.model("Task", taskSchema);