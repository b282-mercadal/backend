// Contains instructions on HOW API will perform its intended tasks
// All operations it can do will be placed in this file

const Task = require("../models/task");

// Controller for getting all the tasks
module.exports.getAllTasks = () => {
	return Task.find({}).then(result => {
		return result;
	});
};


//Controller for creating a task
module.exports.createTask = (requestBody) => {
	let newTask = new Task ({
		name: requestBody.name
	})
	return newTask.save().then((task, error) => {
		if(error){
			console.log(error)
			return false;
		} else {
			return task
		};
	});
} 

// Controller for deleting a task

module.exports.deleteTask = (taskID) => {
	return Task.findByIdAndRemove(taskID).then((removedTask, err) =>{
		if(err){
			console.log(err);
			return false;
		}else{
			return "Deleted task."
		};
	});
};

// Controller for updating a task

module.exports.updateTask = (taskID, newContent) => {
	return Task.findById(taskID).then((result, err) =>{
		if(err){
			console.log(err);
			return false;
		}

		result.name = newContent.name

		return result.save().then((updatedTask, saveErr)=>{
			if(saveErr){
				console.log(saveErr);
				return false;
			}else {
				return "Task updated.";
			};
		});
	});
};

//[SECTION] Activity s36

// Controller for getting specific task
module.exports.getSpecificTask = (taskID) => {
	return Task.findById(taskID).then(result => {
		return result;
	});
};

// Controller for updating specific task

module.exports.updateSpecificTask = (taskID,newContent) => {
	return Task.findById(taskID).then((result, err) => {
		if(err){
			console.log(err);
			return false;
		}

		result.status = newContent.status

		return result.save()

		;
	});
};