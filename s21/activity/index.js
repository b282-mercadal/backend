let users = ["Dwayne Johnson","Steve Austin","Kurt Angle","Dave Bautista"];

console.log("Original Array:")
console.log(users);

// 1. addItem

function addItem(name){
	users[users.length] = name
	console.log(users);
}
addItem("John Cena");
addItem("The Rock");

// 2. getItemByIndex

let itemFound = '';

function getItemByIndex(index){
	return users [index];
}
itemFound = getItemByIndex(3);
console.log (itemFound);

// 3. deleteItem

let deletedItem = '';
function deleteItem(){
	let deletedItem = users[users.length-1];
	users.length = users.length--;
	users.length--;
	return deletedItem; 
}
deletedItem=deleteItem();
console.log("the deleted item is: "+deletedItem)
console.log(users);


// u4. updateItemByIndex

function updateItemByIndex(index,update){
	users[index]=update;
}

updateItemByIndex(3,'The Undertaker');
console.log(users);
updateItemByIndex(5,'Brock Lesnar');
console.log(users);


// 5. 

function deleteAll(){
	let arrLength = users.length;
	for (let i=0; i<arrLength;i++){
		users.length--;
		}
}
deleteAll();
console.log(users);

// 6. let isUsersEmpty;
function isEmpty(){
	if (users.length > 0) {
		return false;
	}
	else {
		return true
	}


}
isUsersEmpty = isEmpty();
console.log(isUsersEmpty);




//Note: Do not change any variable and function names. All variables and functions to be checked are listed in the exports.
try{
    module.exports = {

        users: typeof users !== 'undefined' ? users : null,
        addItem: typeof addItem !== 'undefined' ? addItem : null,
        getItemByIndex: typeof getItemByIndex !== 'undefined' ? getItemByIndex : null,
        deleteItem: typeof deleteItem !== 'undefined' ? deleteItem : null,
        updateItemByIndex: typeof updateItemByIndex !== 'undefined' ? updateItemByIndex : null,
        deleteAll: typeof deleteAll !== 'undefined' ? deleteAll : null,
        isEmpty: typeof isEmpty !== 'undefined' ? isEmpty : null,

    }
} catch(err){

}