//console.log("hello!");

console.log("getUserInfo();");
function getUserInfo () {
	let getUserInfo = {
		name: "John Doe",
		age: 25,
		address: "123 Street, Quezon City",
		isMarried: false,
		petName: "Danny",
	}

	console.log(getUserInfo);
}
getUserInfo();

console.log("getArtistsArray();");
function getArtistsArray () {
	let getArtistsArray = ["Ben & Ben", "Arthur Nery", "Linkin Park", "Paramore", "Taylor Swift"]
	console.log(getArtistsArray);
}

getArtistsArray();

console.log("getSongsArray();");
function getSongsArray () {
	let getSongsArray = ["Kathang Isip", "Binhi", "In The End", "Bring by Boring Brick", "Love Story"]
	console.log(getSongsArray);
}

getSongsArray();

console.log("getMoviesArray();");
function getMoviesArray () {
	let getMoviesArray = ["Lion King", "Meet the Robinsons", "Howls Moving Castle", "Tangled", "Frozen"]
	console.log(getMoviesArray);
}

getMoviesArray();

console.log("getPrimeNumbersArray();");
function getPrimeNumbersArray () {
	let getPrimeNumbersArray = [2, 3, 5, 7, 17]
	console.log(getPrimeNumbersArray);
}

getPrimeNumbersArray();


