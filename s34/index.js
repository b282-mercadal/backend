// Use "require" directive to load the express module/package

const express = require("express");


// Create an application using express
// "app" will serve as our server
const app = express();

const port = 3000;

// Methods used from express.js are middleware
// Middleware is software that provides services and capabilities to applications outside of what's offered by the operating system

// allows your app to read json data
app.use(express.json());

// allows your app to read data from any forms
app.use(express.urlencoded({extended: true}));


// [SECTION] Routes
// GET
app.get("/greet", (request, response) => {
	response.send("Hello from the /greet endpoint!")
});

// POST
app.post("/hello", (request,response) => {
	response.send (`Hello there, ${request.body.firstName} ${request.body.lastName}!`)
})

// Simple registration

let users = [];

app.post("/signup", (request,response) => {
	if(request.body.username !== "" && request.body.password !== "") {
		users.push(request.body);

		response.send(`User ${request.body.username} successfully registered`);
	} else {
		response.send("Please input BOTH username and password")
	}
});


// [SECTION]s34 Activity

// GET Home
app.get("/home", (request, response) => {
	response.send("Welcome to the home page")
});


// GET Users

users = [{
	username: "johndoe",
	password: "johndoe1234"
}];

app.get("/users", (request,response) =>{
		response.send(`${JSON.stringify(users)}`)
});






app.listen(port, () => console.log(`Server running at port ${port}`));