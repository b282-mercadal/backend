//console.log("Hello");

// console.log("Hello World");

//Strictly Follow the property names and spelling given in the google slide instructions.
//Note: Do not change any variable and function names. 
//All variables and functions to be checked are listed in the exports.

// Create an object called trainer using object literals

let trainer = {
	Name: "Ash Ketchum",
	Age: 10,
	Pokemon: ['Pikachu', 'Charizard', 'Squirtle', 'Bulbasaur'],
	Friends: {
		hoenn: ['May', 'Max'],
		kanto: ['Brock', 'Misty']
	},
	talk: function(){
		console.log("Pikachu! I choose you!");
	}
}
console.log(trainer);
// Access object properties using dot notation
console.log("Result of dot notation:");
console.log(trainer.Name);
// Access object properties using square bracket notation
console.log("Result of square bracket notation:");
console.log(trainer['Pokemon']);
// Initialize/add the given object properties and methods

// Properties

// Methods

// Check if all properties and methods were properly added



// Access the trainer "talk" method
console.log("Result of talk method:");
trainer.talk();

// Create a constructor function called Pokemon for creating a pokemon

function Pokemon (name, level) {

	// properties
	this.name = name;
	this.level = level; 
	this.health = 2 * level;
	this.attack = level;

	//method
	this.tackle = function(target){
		console.log(this.name + ' tackled ' + target.name);
		console.log(target.name +"'s health is now reduced to " + Number(target.health - this.attack));
		
		if (target.health <= 0 ) {
			target.faint();
		}
	};
	this.faint = function(){
		console.log(target.name + ' fainted.');
	};

}


// Create/instantiate a new pokemon

let pikachu = new Pokemon('Pikachu', 12);
console.log(pikachu)


// Create/instantiate a new pokemon

let geodude = new Pokemon('Geodude', 8);
console.log(geodude);

// Create/instantiate a new pokemon

let mewtwo = new Pokemon('Mewtwo', 100);
console.log(mewtwo);

// Invoke the tackle method and target a different object

geodude.tackle(pikachu);


// Invoke the tackle method and target a different object
mewtwo.tackle(geodude);

//geodude.faint();






//Do not modify
//For exporting to test.js
//Note: Do not change any variable and function names. All variables and functions to be checked are listed in the exports.
try{
    module.exports = {

        trainer: typeof trainer !== 'undefined' ? trainer : null,
        Pokemon: typeof Pokemon !== 'undefined' ? Pokemon : null

    }
} catch(err){

}