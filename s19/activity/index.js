//console.log("hello")


function login (username, password, role) {

	
	if (!username && !password && !role) {
		return'Inputs must not be empty';
	} else if (!username && password && role) {
		return 'Inputs must not be empty';
	} else if (username && !password && role) {
		return 'Inputs must not be empty';
	} else if (username && password && !role) {
		return 'Inputs must not be empty';
	} else {
		switch (role) {
			case "admin":
				return 'Welcome back to the class portal, admin!'
				break;
			case "teacher":
				return 'Thank you for logging in, teacher!'
				break;
			case "student":
				return 'Welcome to the class portal, student!'
				break;
			default:
				return 'Role out of range.'
	
		}
	
	}
	
}

// average

function checkAverage (num1, num2, num3, num4) {
	const average = Math.round((num1 + num2 + num3 + num4) / 4);
	
	if (average <= 74) {
		return "Hello, student, your average is " + average + "." + " The letter equivalent is F"
	} else if (average >= 75 && average <= 79) {
		return "Hello, student, your average is " + average + "." + " The letter equivalent is D"
	} else if (average >= 80 && average <= 84) {
		return "Hello, student, your average is " + average + "." + " The letter equivalent is C"
	} else if (average >= 85 && average <= 89) {
		return "Hello, student, your average is " + average + "." + " The letter equivalent is B"
	} else if (average >= 90 && average <= 95) {
		return "Hello, student, your average is " + average + "." + " The letter equivalent is A"
	} else (average > 96) 
		return "Hello, student, your average is " + average + "." + " The letter equivalent is A+"
	
}


//Do not modify
//For exporting to test.js
//Note: Do not change any variable and function names. All variables and functions to be checked are listed in the exports.
try{
    module.exports = {

        login: typeof login !== 'undefined' ? login : null,
        checkAverage: typeof checkAverage !== 'undefined' ? checkAverage : null

    }
} catch(err){

}