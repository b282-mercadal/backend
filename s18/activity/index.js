//console.log("hello");

// 1. addNum

function addNum(num1, num2) {
	let sum = num1 + num2;
	console.log("Displayed sum of " + num1 + ' ' + 'and' + ' ' + num2);
	console.log(sum);
}
addNum(5,15);

// 2. subNum

function subNum(num1, num2) {
	let difference = num1 - num2;
	console.log("Displayed difference of " + num1 + ' ' + 'and' + ' ' + num2);
	console.log(difference);
}
subNum(20,5);

// 3. multiplyNum

function multiplyNum(num1, num2) {
	let product = num1 * num2;
	console.log("Displayed product of " + num1 + ' ' + 'and' + ' ' + num2);
	console.log(product);
}
multiplyNum(50,10);

// 4. divideNum

function divideNum(num1, num2) {
	let quotient = num1 / num2;
	console.log("Displayed quotient of " + num1 + ' ' + 'and' + ' ' + num2);
	console.log(quotient);
}
divideNum(50,10);

// 5. getCircleArea

function getCircleArea(radius) {
	let circleArea = radius * radius * Math.PI
	console.log("The result of getting the area of a circle with " + radius + ' ' + "radius:");
	console.log(circleArea);
}
getCircleArea(15);
getCircleArea(20.5);

// 6. getAverage

function getAverage(num1, num2, num3, num4) {
	let averageVar = (num1 + num2 + num3 + num4)/4;
	console.log("The average of " + num1 +','+ num2 +','+ num3 +',' + num4);
	console.log(averageVar);
}
getAverage(20, 40, 60, 80);

// 7. checkIfPassed

function checkIfPassed(score1,score2){
	let percentage = (score1/score2)*100;
	let isPassed = percentage >= 75;
	console.log("Is " + score1 + "/" + score2 + " a passing score?");
	console.log(isPassed);
}

checkIfPassed(38,50);



//Do not modify
//For exporting to test.js
//Note: Do not change any variable and function names. All variables and functions to be checked are listed in the exports.
try{
	module.exports = {

		addNum: typeof addNum !== 'undefined' ? addNum : null,
		subNum: typeof subNum !== 'undefined' ? subNum : null,
		multiplyNum: typeof multiplyNum !== 'undefined' ? multiplyNum : null,
		divideNum: typeof divideNum !== 'undefined' ? divideNum : null,
		getCircleArea: typeof getCircleArea !== 'undefined' ? getCircleArea : null,
		getAverage: typeof getAverage !== 'undefined' ? getAverage : null,
		checkIfPassed: typeof checkIfPassed !== 'undefined' ? checkIfPassed : null,

	}
} catch(err){

}