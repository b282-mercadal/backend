//console.log("hello");

//Functions
function printInput(){
	let nickname = prompt("Enter your nickname:");
	console.log("Hi, " + nickname);
}

printInput();

// Parameters and Arguments

// (name)- is called a parameter

function printName(name) {
	console.log("My name is " + name);
}
printName("Juana");